<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'tshewangdaluka108@gmail.com',
                'role' => 'admin',
                'collegeid' => 'admin',
                'contact' => '17665047',
                'password' => bcrypt('password')
            ],
            [
                'name' => 'Tshewang Dechen',
                'email' => '12200092.gcit@rub.edu.bt',
                'role' => 'user',
                'collegeid' => '12200092',
                'contact' => '17301649',
                'password' => bcrypt('password')
            ],

        ]);

        
    }
}
