<?php

namespace App\Http\Controllers\BackPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Feedback;
use App\Models\Facility;
use App\Models\Bookings;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function dashboard(){
        $usercount = User::count();
        $facilitycount = Facility::count();
        $feedbackcount = Feedback::count();
        $bookingcount = Bookings::count();
        return view('admin.dashboard', compact('usercount', 'facilitycount','feedbackcount','bookingcount'));
    }
    public function bookingsChart()
    {
        $facilities = Facility::all();
        $data = [];

        foreach ($facilities as $facility) {
            $count = Bookings::where('facility_id', $facility->id)->count();
            $data['labels'][] = $facility->fname;
            $data['counts'][] = $count;
        }
        return response()->json($data);
    }

    public function facility()
    {
        $facilities = Facility::all();
        return view('admin.facility', compact('facilities'));    
    }

    // // post or create functionality
    // public function addfacility(Request $request) {
    //     $imagePath = $request->file('image')->store('public/images');
    //     Facility::create([
    //         'fname' => $request->fname,
    //         'maxtime' => $request->maxtime,
    //         'category' => $request->category,
    //         'description' => $request->description,
    //         'starttime' => $request->starttime,
    //         'endtime' => $request->endtime,
    //         'image' => $imagePath,
    //     ]);
    //     return redirect()->route('admin.facility');
    // }
    public function addfacility(Request $request)
{
    try {
        // Validate the request data as needed

        // Store the image
        $imagePath = $request->file('image')->store('public/images');

        // Create a new Facility
        Facility::create([
            'fname' => $request->fname,
            'maxtime' => $request->maxtime,
            'category' => $request->category,
            'description' => $request->description,
            'starttime' => $request->starttime,
            'endtime' => $request->endtime,
            'image' => $imagePath,
        ]);

        // Add a success message to the session
        return redirect()->route('admin.facility')->with('success', 'Facility added successfully');
    } catch (\Exception $e) {
        // Handle any errors that occur during the process

        // Add an error message to the session
        return redirect()->route('admin.facility')->with('error', 'Error adding facility');
    }
}
    
    public function facilitydelete(Request $request)
    {
        $categoryIds = $request->input('categoryIds');    
        try {
            //  delete facility by IDs
            Facility::whereIn('id', $categoryIds)->delete();  
            return response()->json(['success' => true, 'message' => 'Facility deleted successfully',  'icon' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Error deleting facility', 'icon' => 'error', 'error' => $e->getMessage()]);
        }
    }

    public function editFacility(Request $request, $id)
    {
    try {
        $facilities = Facility::findOrFail($id);
        if ($request->hasFile('image')) {
            $facilities->image = $request->file('image')->store('images', 'public');
        }
        $facilities->update($request->except('image'));

        return redirect()->route('admin.facility')->with('success', 'Facility updated successfully');
        } catch (\Exception $e) {
            return redirect()->route('admin.facility')->with('error', 'Error updating facility');
        }
    }

    public function booking()
    {
        $bookings = Bookings::all();
        return view('admin.bookings', compact('bookings'));
    }


    public function feedback()
    {
        $feedback = Feedback::all();
        return view('admin.feedback', compact('feedback'));
    }

    public function feedbackdelete(Request $request)
    {
        $categoryIds = $request->input('categoryIds');
    
        try {
            //  delete feedbacks by IDs
            Feedback::whereIn('id', $categoryIds)->delete();  
            return response()->json(['success' => true, 'message' => 'Feedbacks deleted successfully',  'icon' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Error deleting feedback', 'icon' => 'error', 'error' => $e->getMessage()]);
        }
    }

    // public function user()
    // {
    //     $users = User::all();
    //     return view('admin.users', compact('users'));
    // }
    public function user()
    {
        // Fetch only users with role 'user'
        $users = User::where('role', 'user')->get();
        return view('admin.users', compact('users'));
    }

    public function userdelete(Request $request)
    {
        $categoryIds = $request->input('categoryIds');
    
        try {
            //  delete users by IDs
            User::whereIn('id', $categoryIds)->delete();  
            return response()->json(['success' => true, 'message' => 'User deleted successfully',  'icon' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Error deleting user', 'icon' => 'error', 'error' => $e->getMessage()]);
        }
    }

    public function searchUsers(Request $request)
    {
        $userName = $request->input('userName');
        $users = DB::table('users')
            ->where('name', 'like', "%$userName%")
            ->where('role', 'user') // Add this condition to filter users with role 'user'
            ->get();

        return view('admin.users', ['users' => $users]);
    }

    public function searchBookings(Request $request)
    {
        try {
            $facilityName = $request->input('facilityName');
            // Add debugging statements
            dd("Search term: $facilityName");

            $bookings = DB::table('bookings')
                ->join('facilities', 'bookings.facility_id', '=', 'facilities.id')
                ->where('facilities.fname', 'like', "%$facilityName%")
                ->get();

            return view('admin.bookings', ['bookings' => $bookings]);
        } catch (\Exception $e) {
            // Log the error for further investigation
            \Log::error($e);

            // Return an error response or redirect as needed
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    public function facilitystatus($facilityId)
    {
        $facility = Facility::find($facilityId);
        $facility->status = ($facility->status === 'active') ? 'disabled' : 'active';
        $facility->save();
    
        return redirect()->back()->with('success', 'Facility status updated successfully. New status: ' . ucfirst($facility->status));
    }

    public function bookingsdelete(Request $request)
    {
        $categoryIds = $request->input('categoryIds');
    
        try {
            Bookings::whereIn('id', $categoryIds)->delete();  
            return response()->json(['success' => true, 'message' => 'Feedbacks deleted successfully',  'icon' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Error deleting feedback', 'icon' => 'error', 'error' => $e->getMessage()]);
        }
    }
}



//     public function facilities()
//     {
//         $categories = Category::all();
//         $subcategories = Subcategory::all();
//         return view('admin.facilities', compact('categories', 'subcategories'));
//     }
//     public function createfacility(Request $request)
//     {
//         // dd($request->all());
//         // DB::enableQueryLog();
//         // dd(DB::getQueryLog());
//         try {
//             $request->validate([
//                 'category_name' => ['required', 'string', 'max:255', 'unique:categories'], 
//                 'description' => ['string', 'max:255'],
//                 'image' => [ 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
//             ]);
    
//             $category = Category::where('category_name', $request->category_name)->first();
        
//             if ($category) {
//                 return redirect()->route('admin.facilities')->with('error', 'This category is already created!');
//             }
    
//             $imagePath = $request->file('image')->store('public/images'); 
//             Category::create([
//                 'category_name' => $request->category_name,
//                 'description' => $request->description,
//                 'image' => $imagePath, // Store the image path in the database
//             ]);
//             return redirect()->route('admin.facilities')->with('success', 'Category has been created');
//         }catch (\Exception $e) {
//             dd($e->getMessage());
//             Log::error('Error creating category: ' . $e->getMessage());
//             return redirect()->route('admin.facilities');
//         }
//     }
    
   
//     public function createsubfacility(Request $request)
// {
    
   
//     // Validate the form data, including the image upload
//     $request->validate([
//         'facility_name' => ['required', 'string', 'max:255'],
//         'category_id' => ['required', 'integer', 'exists:categories,id'],
//         'image' => ['required', 'image', 'mimes:jpeg,png,jpg', 'max:2048'], // Adjust validation rules for image files
//     ]);

//     // Handle the image upload and save it to a storage location
//     $imagePath = $request->file('image')->store('images', 'public'); // Adjust the storage location as needed

//     // Check if the subcategory already exists
//     $subcategory = Subcategory::where('facility_name', $request->facility_name)->first();

//     if ($subcategory) {
//         // Subcategory already exists, show an error message
//         return redirect()->route('admin.facilities')->with('error', 'This facility is already created!');
//     }

//     // If the subcategory does not exist, create it
//     Subcategory::create([
//         'facility_name' => $request->facility_name,
//         'category_id' => $request->category_id,
//         'resource' => $request->resource,
//         'time' => $request->time,
//         'image' => $imagePath, // Store the image path in the database
//     ]);

//     return redirect()->route('admin.facilities')->with('success', 'Facility has been created');
// }




//     public function user()
//     {
//         $users = User::all();
//         return view('admin.user', compact('users'));
//     }
//     public function createuser(Request $request)
//     {
//         // dd($name, $email, $usertype, $role); 
   
//         $request->validate([
//             'name' => ['required', 'string', 'max:255'],
//             'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
//             // 'usertype' => ['required', 'string', 'max:255'],
//             // 'role' => ['required', 'integer', 'max:255'],
//             // 'password' => ['required', Rules\Password::defaults()],
//         ]);
//         $user = User::where('email', $request->email)->first();

//         if ($user) {
//             // Subcategory already exists, show an error message
//             return redirect()->route('admin.user')->with('error', 'This user already exist!');
//         }
    
//         // Create the user
//         $user = User::create([
//             'name' => $request->name,
//             'email' => $request->email,
//             'usertype' => $request->usertype,
//             'role' => $request->role,
//         ]);
//             // User creation failed, handle the error and return an error message
//         return redirect()->route('admin.user')->with('success', 'User is successfully added.');
        
//     }
//     public function deleteUsers($id) {
      
//         $user = User::findOrFail($id);
//         $user->delete();
//             // Redirect back with a success message
//         return redirect()->route('admin.user')->with('success', 'User is successfully deleted.');
      
//     }


    
// }