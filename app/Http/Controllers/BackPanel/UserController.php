<?php

namespace App\Http\Controllers\BackPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Feedback;
use App\Models\User;
use App\Models\Facility;
use App\Models\Bookings;
use Illuminate\Support\Facades\Auth;
use App\Notifications\BookingCancelled;
use Illuminate\Support\Facades\Notification; // Fix the namespace here


class UserController extends Controller
{
    public function index(){
        return view('index');
    }
    public function about(){
        return view('about');
    }
    public function Dashboard(){
        // dd($request->all());
        $feedback = Feedback::latest()->take(10)->get();
        $facilities = Facility::all();

        return view('dashboard', compact('feedback','facilities'));
    }
    public function Bookings(){
        $bookings = Bookings::all();
        return view('bookings', compact('bookings'));
    }

    public function cancelbooking($id)
    {
        try {
            $booking = Bookings::find($id);
            if (!$booking) {
                session()->flash('error', 'Booking not found');
                return redirect()->back();
            }
            // Check if the user has permission to cancel this booking
            if (auth()->user()->id != $booking->user->id) {
                session()->flash('error', 'Permission denied');
                return redirect()->back();
            }
            // // Log information for debugging
            // \Log::info('Canceling Booking', [
            //     'user_id' => auth()->user()->id,
            //     'booking_id' => $booking->id,
            //     'facility_name' => $booking->facility->fname,
            //     'booking_date' => $booking->bookingdate,
            // ]);

            // Prepare data for the notification
            $notificationData = [
                'id' => $booking->id,
                'facility' => [
                    'fname' => $booking->facility->fname,
                    // Add other facility details if needed
                ],
                'bookingdate' => $booking->bookingdate,
                'starttime' => $booking->starttime,
                'endtime' => $booking->endtime,
            ];
            // Attempt to delete the booking and send notifications
            $deleted = $booking->delete();
            
            if ($deleted) {
                // Mark all unread notifications as read for the current user
                auth()->user()->unreadNotifications->markAsRead();
                
                // Send notification to all users (except the one canceling the booking)
                Notification::send(User::where('id', '!=', $booking->user->id)->get(), new BookingCancelled($notificationData));

                session()->flash('success', 'Booking canceled successfully');
            } else {
                session()->flash('error', 'Error canceling booking 3');
            }
            return redirect()->back();
        } catch (\Exception $e) {
            // Log the exception for further analysis
            \Log::error('Error canceling booking2', ['exception' => $e->getMessage()]);
            
            session()->flash('error', 'Error canceling booking 22');
            return redirect()->back();
        }
    }
    
    public function feedback(Request $request) {
        try {
            Feedback::create([
                'user_id' => Auth::id(),
                'feedback' => $request->feedback,
            ]);
            return back()->with('success', 'Feedback submitted successfully!');
        } catch (\Exception $e) {
            return back()->with('error', 'An error occurred while submitting feedback. Please try again.');
        }
    }
    

    public function addbooking(Request $request)
    {
    try {
        list($starttime, $endtime) = explode(',', $request->time);
        Bookings::create([
            'facility_id' => $request->facility_id,
            'user_id' => Auth::id(),
            'bookingdate' => $request->bookingdate,
            'starttime' => $starttime,
            'endtime' => $endtime,
        ]);
        // Use session to store success message
        session()->flash('success', 'Your booking was succesful');
        return redirect()->route('facilities');
    } catch (QueryException $exception) {
        // This exception will be thrown if there's a database constraint violation
        // For example, if the selected time slot is already booked
        session()->flash('error', 'Failed to add booking. The selected time slot may already be booked.');
        return redirect()->back();
    } catch (\Exception $exception) {
        session()->flash('error', 'Failed to add booking. The selected time slot may already be booked.');
        return redirect()->back();
    }
    }

    public function Facility(){
        $facilities = Facility::all();
        $bookings = Bookings::latest()->take(10)->get();
        return view('facilities', compact('facilities', 'bookings'));
    }

    public function getTime(Request $request, $subfacilityId)
    {
        // Fetch the subcategory session dates based on the subcategory ID
        $subcategory = Facility::findOrFail($subfacilityId);
        $startDate = $request->query('start_date');
    
        // Filter Booking model based on subcategory_id and date range
        $bookings = Bookings::where('facility_id', $subfacilityId)
            ->where('bookingdate', $startDate)
            ->get();

        $bookingStartTimes = $bookings->pluck('starttime')->toArray();
        $starttime = $subcategory->starttime;
        $endtime = $subcategory->endtime;
    
        $slot = $subcategory->maxtime;
        $bhutanTime = new \DateTime('now', new \DateTimeZone('Asia/Thimphu'));
        $bhutanCurrentTime = $bhutanTime->format('Y-m-d H:i:s');
    
        $response = [
            'session_start_time' => $starttime,
            'session_end_time' => $endtime,
            'booking_start_times' => $bookingStartTimes,
            'slot' => $slot,
            'bhutan_current_time' => $bhutanCurrentTime,
        ];
    
        return response()->json($response);
    }
    
    

}
