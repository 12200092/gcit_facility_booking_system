<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingCancelled extends Notification
{
    use Queueable;

    protected $booking;

    public function __construct($booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param object $notifiable
     * @return array<int, string>
     */
    public function via($notifiable)
    {
        return ['database']; // Add 'database' as a delivery channel
    }

    // /**
    //  * Get the mail representation of the notification.
    //  *
    //  * @param object $notifiable
    //  * @return MailMessage
    //  */
    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //         ->line('The booking for ' . $this->booking['facility']['fname'] . ' on ' . $this->booking['bookingdate'] . ' has been cancelled.') // Include booking details
    //         ->action('View Booking', url('/bookings'))
    //         ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param object $notifiable
     * @return array<string, mixed>
     */
    public function toDatabase($notifiable)
    {
        return [
            'booking_id' => $this->booking['id'] ?? null, // Use array access to get the booking ID
            'message' => 'The booking for ' . ($this->booking['facility']['fname'] ?? '') . ' on ' . ($this->booking['bookingdate'] ?? '') . ' from ' . ($this->booking['starttime'] ?? '') . ' to ' . ($this->booking['endtime'] ?? '') . ' has been cancelled and is now open for booking.',
        ];
    }
}
