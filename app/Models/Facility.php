<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    use HasFactory;

    protected $fillable = [
        'fname',
        'maxtime',
        'category',
        'description',
        'starttime',
        'endtime',
        'image',
        'status',
    ];

    public function bookings() 
    {
        return $this->hasMany(Bookings::class, 'facility_id');
    }
}
