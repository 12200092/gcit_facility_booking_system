<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'collegeid',
        'contact',
        'password',
    ];

    public function feedbacks() 
    {
        return $this->hasMany(Feedback::class, 'user_id');
    }
    public function bookings() 
    {
        return $this->hasMany(Booking::class, 'user_id');
    }

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function getInitialsAttribute()
    {
        $nameParts = explode(' ', $this->name);

        $firstNameInitial = strtoupper(substr($nameParts[0], 0, 1));
        $lastNameInitial = isset($nameParts[1]) ? strtoupper(substr($nameParts[1], 0, 1)) : '';

        return $firstNameInitial . $lastNameInitial;
    }
}
