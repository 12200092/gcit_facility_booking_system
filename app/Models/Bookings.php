<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    use HasFactory;

    protected $fillable = [
        'facility_id',
        'user_id',
        'bookingdate',
        'starttime',
        'endtime',
    ];
    public function facility(){
        return $this -> belongsTo(Facility::class, 'facility_id');
    }
    public function user(){
        return $this -> belongsTo(User::class, 'user_id');
    }
   


}
