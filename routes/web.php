<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BackPanel\AdminController;
use App\Http\Controllers\BackPanel\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', function () {
    return view('home');
});

// Route::get('/index', function () {
//     return view('index');
// })->middleware(['auth', 'verified'])->name('index');

// Route::get('/about', function () {
//     return view('about');
// })->middleware(['auth', 'verified'])->name('about');


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::middleware('auth','role:admin')->group(function () {
    // Route::get('admin/dashboard', [AdminController::class, 'dashboard']);
    Route::get('admin/dashboard', [AdminController::class, 'dashboard'])->middleware('verified');
    Route::get('admin/facility', [AdminController::class, 'facility'])->name('admin.facility');
    Route::get('admin/booking', [AdminController::class, 'booking']);
    Route::get('admin/feedback', [AdminController::class, 'feedback']);
    Route::get('admin/users', [AdminController::class, 'user'])->name('admin.users');

    Route::delete('admin/users/delete', [AdminController::class, 'userdelete'])->name('users.destroy');
    Route::delete('admin/feedbacks/delete', [AdminController::class, 'feedbackdelete'])->name('feedback.destroy');
    Route::delete('admin/facility/delete', [AdminController::class, 'facilitydelete'])->name('facility.destroy');
    Route::post('admin/addfacility', [AdminController::class, 'addfacility'])->name('addfacility');
    Route::put('admin/facility/update/{id}', [AdminController::class, 'editfacility'])->name('facility.edit');
    Route::delete('admin/bookings/delete', [AdminController::class, 'bookingsdelete'])->name('bookings.destroy');

    Route::get('/bookings/chart', [AdminController::class, 'bookingsChart']);
    Route::get('/searchUsers', [AdminController::class, 'searchUsers'])->name('searchUsers');
    Route::get('/searchBookings', [AdminController::class, 'searchBookings'])->name('searchBookings');
    Route::get('/facilitystatus/{facilityId}', [AdminController::class, 'facilitystatus'])->name('facilitystatus');

});


Route::middleware('auth','role:user')->group(function () {
    Route::get('/index', [UserController::class, 'index'])->middleware('verified')->name('index');
    // Route::get('/home', [UserController::class, 'Dashboard'])->name('home');
    Route::get('/home', [UserController::class, 'Dashboard'])->middleware('verified')->name('home');
    Route::get('/about', [UserController::class, 'about'])->name('about');
    Route::get('/facilities', [UserController::class, 'Facility'])->name('facilities');
    Route::get('/bookings', [UserController::class, 'Bookings'])->name('bookings');
    Route::post('/home/feedback', [UserController::class, 'Feedback'])->name('feedback');
    Route::post('/facilities/addbooking', [UserController::class, 'addbooking'])->name('addbooking');
    Route::delete('/cancelbooking/{id}', [UserController::class, 'cancelbooking'])->name('cancelbooking');
    Route::get('/getTime/{subfacilityId}', [UserController::class, 'getTime'])->name('getTime');
});

