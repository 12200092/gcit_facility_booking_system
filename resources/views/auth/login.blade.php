<x-guest-layout> 
    {{-- <div class="login-container"> --}}
        <form method="POST" class="form" action="{{ route('login') }}">
            <div class="start-text mb-4">
                <h3>Welcome Back!</h3>
                <p>Please login to keep connected with us.</p>
            </div>
            <x-auth-session-status :status="session('status')" />

            @csrf
            <div class="email">
                <x-input-label for="email" :value="__('Email')"/>
                <input id="email" type="email" name="email" :value="old('email')" required autofocus autocomplete="username" />
                <x-input-error :messages="$errors->get('email')" class="mt-2" />
            </div>
            <div class="password">
                <x-input-label for="password" :value="__('Password')"/>
                <input id="password" type="password" name="password" required autocomplete="current-password" />
                <x-input-error :messages="$errors->get('password')" class="mt-2" />
            </div>

            <div class="mb-3 d-flex justify-content-around">
                <label for="remember_me" class="me-4">
                    <input id="remember_me" type="checkbox" class="rounded dark:bg-gray-900 border-gray-300 dark:border-gray-700 text-indigo-600 shadow-sm focus:ring-indigo-500 dark:focus:ring-indigo-600 dark:focus:ring-offset-gray-800" name="remember">
                    <span class="ms-2 text-gray-600 dark:text-gray-400">{{ __('Remember me') }}</span>
                </label>
                @if (Route::has('password.request'))
                    <a class="ms-4 text-decoration-none text-indigo-600 " href="{{ route('password.request') }}">
                        {{ __('Forgot password?') }}
                    </a>
                @endif
            </div> 
            <button class="login-btn">Login</button>
            <div class="end-text">
                Don't have an account?<a href="/register" class="ms-2 text-decoration-none text-indigo-600"> Sign Up</a>
            </div>
        </form>
    {{-- </div> --}}
    
</x-guest-layout>

