<x-guest-layout>
    <form method="POST" class="form" action="{{ route('register') }}">
        <div class="start-text mb-4">
            <h3>Hello, everyone!</h3>
            <p>Enter personal details & start your journey with us!</p>
        </div>
        @csrf
        <div class="name">
            <x-input-label for="name" :value="__('Name')"/>
            <input id="name" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            <x-input-error :messages="$errors->get('name')" class="mt-2" />
        </div>
        <div class="collegeid">
            <x-input-label for="collegeid" :value="__('CollegeID')"/>
            <input id="collgeid" type="text" name="collegeid" :value="old('collgeid')" required autocomplete="username" />
            <x-input-error :messages="$errors->get('collgeid')" class="mt-2" />
        </div>
        <div class="email">
            <x-input-label for="email" :value="__('Email')"/>
            <input id="email" type="email" name="email" :value="old('email')" required autocomplete="username" />
            <x-input-error :messages="$errors->get('email')" class="mt-2" />
        </div>
        <div class="contact">
            <x-input-label for="contact" :value="__('Contact')"/>
            <input id="contact" type="text" name="contact" :value="old('contact')" required autocomplete="username" />
            <x-input-error :messages="$errors->get('contact')" class="mt-2" />
        </div>
        <div class="password">
            <x-input-label for="password" :value="__('Password')"/>
            <input id="password"type="password" name="password" required autocomplete="new-password" />
            <x-input-error :messages="$errors->get('password')" class="mt-2" />
        </div>
        <div class="cpassword">
            <x-input-label for="password_confirmation" :value="__('Confirm Password')"/>
            <input id="password_confirmation" type="password" name="password_confirmation" required autocomplete="new-password" />
            <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
        </div>
            
        <button class="register-btn">Sign Up</button>
        
        <div class="end-text">
            Already have an account?
            <a class="ms-2 text-decoration-none text-indigo-600" href="{{ route('login') }}">Sign In</a>
        </div>  
    </form>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            @if(session('status'))
                // Display a toast message using your preferred toast library or custom implementation
                // For example, using a library like Toastr.js
                toastr.success("{{ session('status') }}");
            @endif
        });
    </script>

</x-guest-layout>
   

