<x-guest-layout>
    <form method="POST" class="form" action="{{ route('password.store') }}">
        <div class="head">
            <h4>Reset your password!</h4>
            <p>"Reset a new password to regain access to your account."<p>
        </div>
        @csrf
        <!-- Password Reset Token -->
        <input type="hidden" name="token" value="{{ $request->route('token') }}">

        <div class="email">
            <x-input-label for="email" :value="__('Email')" />
            <input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email', $request->email)" required autofocus autocomplete="username" />
            <x-input-error :messages="$errors->get('email')" class="mt-2" />
        </div>

        <div class="password">
            <x-input-label for="password" :value="__('New Password')" />
            <input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            <x-input-error :messages="$errors->get('password')" class="mt-2" />
        </div>

        <div class="cpassword">
            <x-input-label for="password_confirmation" :value="__('Confirm Password')" />
            <input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
        </div>

        <button class="forgot-btn mt-4">
            {{ __('Reset Password') }}
        </button>
    </form>
</x-guest-layout>
