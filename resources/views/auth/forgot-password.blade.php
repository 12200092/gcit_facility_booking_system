<x-guest-layout>
    <form method="POST" class="form" action="{{ route('password.email') }}">
        <div class="head">
            <h4>Forgot your password?</h4>
            <p>Don't worry, we'll send you an email to reset your password.<p>
        </div>
        <x-auth-session-status class="mb-4" :status="session('status')" />

        @csrf

        <div>
            <x-input-label for="email" :value="__('Email')" />
            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            <x-input-error :messages="$errors->get('email')" class="mt-2" />
        </div>

        <div class="flex mt-4 gap-4">
            <button class="forgot-btn">
                {{ __('Cancel') }}
            </button>
            <button class="forgot-btn">
                {{ __('Submit') }}
            </button>
        </div>
    </form>
</x-guest-layout>

{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token() }}">
    <title>Forgot Password</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/forgot_password.css')}}">
</head>
<body>
    <div class="forgot-password-container">
        <x-auth-session-status class="mb-4" :status="session('status')" />
        <form method="POST" action="{{ route('password.email') }}">
            <div class="head">
                <h4>Forgot your password?</h4>
                <p>Don't worry, we'll send you an email to reset your password.<p>
            </div>
            @csrf
    
            <div class="email">
                <x-input-label for="email" :value="__('Email')"/>
                <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                <x-input-error :messages="$errors->get('email')" class="mt-2" />
            </div>
    
            <div class="d-flex gap-5">
                <button class="forgot-btn">
                    {{ __('Cancel') }}
                </button>
                <button class="forgot-btn">
                    {{ __('Submit') }}
                </button>
            </div>

        </form>
    </div>   
</body>
</html> --}}