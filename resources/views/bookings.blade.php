{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Bookings</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/bookings.css')}}">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11">
</head>
<body>
    <header class="bookings-head">
        <h5>My Bookings</h5>
    </header>
    @include('include.navbar')

    @forelse($bookings as $b)
    <div class="user-container">
        @if(auth()->user()->id == $b->user->id)
            <div class="display:flex; flex-direction:column">
                <p><span>Facility Name: </span>{{ $b->facility->fname }}</p>
                <p><span>Booked Date: </span>{{ $b->bookingdate }}</p>
                <p><span>Booking Time: </span>{{ $b->starttime }} to {{ $b->endtime }}</p>
            </div>
            <form id="cancelForm_{{ $b->id }}" action="{{ route('cancelbooking', $b->id) }}" method="post">
                @csrf
                @method('DELETE')
                <div class="cancel-btn">
                    <button type="button" onclick="confirmCancellation({{ $b->id }})">Cancel Booking</button>
                </div>
            </form>
        @endif    
    </div>  
@empty
    <div class="no-booking">You have no bookings currently.</div>
@endforelse
 

    <!-- Include SweetAlert JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        function confirmCancellation(bookingId) {
        Swal.fire({
            title: 'Are you sure you want to cancel this booking?',
            text: 'You won\'t be able to revert this!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#06bc0b',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, cancel it!'
        }).then((result) => {
            if (result.isConfirmed) {
                // Submit the form for cancellation
                document.getElementById('cancelForm_' + bookingId).submit();
            }
        });
    }
        @if(session('success'))
            Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: '{{ session('success') }}',
                showConfirmButton: false,
                timer: 1500
            });
        @endif

        @if(session('error'))
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: '{{ session('error') }}',
                showConfirmButton: false,
                timer: 1500
            });
        @endif
    </script>

</body>
</html> --}}


@extends('index')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/bookings.css')}}">
    
@endsection
@section('content')

@include('include.navbar')
<section class="booking-container">
    @forelse($bookings as $b)
    <div class="user-container">
        @if(auth()->user()->id == $b->user->id)
            <div class="display:flex; flex-direction:column">
                <p><span>Facility Name: </span>{{ $b->facility->fname }}</p>
                <p><span>Booked Date: </span>{{ $b->bookingdate }}</p>
                <p><span>Booking Time: </span>{{ $b->starttime }} to {{ $b->endtime }}</p>
            </div>
            <form id="cancelForm_{{ $b->id }}" action="{{ route('cancelbooking', $b->id) }}" method="post">
                @csrf
                @method('DELETE')
                <div class="cancel-btn">
                    <button type="button" onclick="confirmCancellation({{ $b->id }})">Cancel Booking</button>
                </div>
            </form>
        @endif    
    </div>  
@empty
    <div class="no-booking">You have no bookings currently.</div>
@endforelse
</section>

    <!-- Include SweetAlert JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        function confirmCancellation(bookingId) {
        Swal.fire({
            title: 'Are you sure you want to cancel this booking?',
            text: 'You won\'t be able to revert this!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#06bc0b',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, cancel it!'
        }).then((result) => {
            if (result.isConfirmed) {
                // Submit the form for cancellation
                document.getElementById('cancelForm_' + bookingId).submit();
            }
        });
    }
        @if(session('success'))
            Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: '{{ session('success') }}',
                showConfirmButton: false,
                timer: 1500
            });
        @endif

        @if(session('error'))
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: '{{ session('error') }}',
                showConfirmButton: false,
                timer: 1500
            });
        @endif
    </script>


@endsection