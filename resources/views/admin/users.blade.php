<x-admin-sidebar>
    <div class="user-container">
        <div class="user-delete">
            <div class="search-user">
                <form action="{{ route('searchUsers') }}" method="GET" id="searchForm">
                    <input type="text" class="form-control pe-5" id="userName" name="userName" placeholder="Search users ..." value="{{ request('userName') }}">
                </form>
            </div>           
            <button type="button" class="btn btn-danger" onclick="fireSweetAlertdelete()" style="background-color: red">Delete User</button>
        </div>
        <div class="user-table">
            <table class="table" >
                <thead >
                  <tr>
                    <th>
                        <span class="custom-checkbox">
                            <input type="checkbox" id="selectAll">
                        </span>
                    </th>
                    <th scope="col">User ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Password</th>
                    <th scope="col">Contact Number</th>
                  </tr>
                </thead>
                <tbody>
                    @forEach($users as $u)
                  <tr>
                    <td>
                        <span class="custom-checkbox">
                            <input type="checkbox" id="{{$u->id}}" name="options[]" value="{{$u->id}}">
                        </span>
                    </td>
                    <td>{{$u->collegeid}}</td>
                    <td>{{$u->name}}</td>
                    <td>{{$u->email}}</td>
                    <td>{{$u->password}}</td>
                    <td>{{$u->contact}}</td>
                  </tr>
                  @endforeach      
                </tbody>
            </table>
        </div>

    </div>   

   
    <script>
        $(document).ready(function () {
          $("#selectAll").change(function () {
            $("input[name='options[]']").prop("checked", $(this).prop("checked"));
          });
          $("input[name='options[]']").change(function () {
            if (!$(this).prop("checked")) {
              $("#selectAll").prop("checked", false);
            }
          });
        });
      </script>

<script>
    function fireSweetAlertdelete() {
       const selectedCategoryIds = [];
       // Use attribute selector to select checkboxes by id
       $('input[name="options[]"]:checked').each(function () {
          selectedCategoryIds.push($(this).val());
       });
       console.log('Selected Category IDs:', selectedCategoryIds);
 
       if (selectedCategoryIds.length === 0) {
          Swal.fire('Error!', 'Please select at least one user to delete', 'error');
          return;
       }
 
       Swal.fire({
          title: 'Are you sure you want to delete this user?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#5cb85c',
          cancelButtonColor: '#d9534f',
          confirmButtonText: 'Yes, delete it!'
       }).then((result) => {
          if (result.isConfirmed) {
             console.log('Before Axios call');
             axios({
                method: 'delete',
                url: 'admin/users/delete',
                data: { categoryIds: selectedCategoryIds }
             })
             .then((response) => {
                console.log('Axios success', response.data);
                
                setTimeout(() => {
                        location.reload();
                    }, 1500);
                Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        text: response.data.message,
                        showConfirmButton: false
                    });   
             })
             .catch((error) => {
                console.log('Axios error', error);
                Swal.fire('Error!', error.response.data.message, 'error');
             });
             console.log('After Axios call');
          }
       });
    }
</script>
<script>
    document.getElementById('userName').addEventListener('input', function() {
        document.getElementById('searchForm').submit();
    });
</script>

</x-admin-sidebar>
