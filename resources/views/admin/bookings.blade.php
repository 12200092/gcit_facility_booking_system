<x-admin-sidebar>
  <div class="user-container">
    <div class="feedback-delete">
      <button type="button" class="btn btn-danger" onclick="fireSweetAlertdelete()" style="background-color: red">Delete Bookings</button>
    </div>
    <div class="user-table">
      <table class="table" >
        <thead >
          <tr>
            <th>
              <span class="custom-checkbox">
                  <input type="checkbox" id="selectAll">
              </span>
            </th>
            <th scope="col">Facility Name</th>
            <th scope="col">Booked by</th>
            <th scope="col">Email</th>
            <th scope="col">Booking Date</th>
            <th scope="col">Booking Time</th>
          </tr>
        </thead>
        <tbody>
            @forEach($bookings as $b)
          <tr>
            <td>
              <span class="custom-checkbox">
                  <input type="checkbox" id="{{$b->id}}" name="options[]" value="{{$b->id}}">
              </span>
            </td>
            <td>{{$b->facility->fname}}</td>
            <td>{{$b->user->name}}</td>
            <td>{{$b->user->email}}</td>
            <td>{{$b->bookingdate}}</td>
            <td>{{$b->starttime}} to {{$b->endtime}}</td>
          </tr>
          @endforeach      
        </tbody>
      </table>
    </div>

  </div>   

  <script>
    $(document).ready(function () {
      $("#selectAll").change(function () {
        $("input[name='options[]']").prop("checked", $(this).prop("checked"));
      });
      $("input[name='options[]']").change(function () {
        if (!$(this).prop("checked")) {
          $("#selectAll").prop("checked", false);
        }
      });
    });
</script>

<script>
  function fireSweetAlertdelete() {
    const selectedCategoryIds = [];
    $('input[name="options[]"]:checked').each(function () {
    selectedCategoryIds.push($(this).val());
    });
    console.log('Selected Category IDs:', selectedCategoryIds);

    if (selectedCategoryIds.length === 0) {
        Swal.fire('Error!', 'Please select at least one booking to delete', 'error');
        return;
    }
    Swal.fire({
        title: 'Are you sure you want to delete this bookings?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#5cb85c',
        cancelButtonColor: '#d9534f',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
          console.log('Before Axios call');
          axios({
              method: 'delete',
              url: 'bookings/delete',
              data: { categoryIds: selectedCategoryIds }
          })
          .then((response) => {
              console.log('Axios success', response.data);
              
              setTimeout(() => {
                      location.reload();
                  }, 1500);
              Swal.fire({
                      icon: 'success',
                      title: 'Success!',
                      text: response.data.message,
                      showConfirmButton: false
                  });   
          })
          .catch((error) => {
              console.log('Axios error', error);
              Swal.fire('Error!', error.response.data.message, 'error');
          });
          console.log('After Axios call');
        }
    });
  }
</script>
  
</x-admin-sidebar>

                 