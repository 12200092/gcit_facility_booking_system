<x-admin-sidebar>
  <div class="user-container">
    <div class="user-delete">
      <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Add Facility</button>
      <button type="button" class="btn btn-danger" onclick="fireSweetAlertdelete()">Delete Facility</button>

      <!-- Modal -->
      <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog ">
          <div class="modal-content p-3">
            <div class="modal-header">
            <h1 class="modal-title fs-5" id="staticBackdropLabel">Add a new facility</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form class="form" action="{{route('addfacility')}}" method="POST" enctype="multipart/form-data"> 
                @csrf
                  <div class="mb-2">
                    <label for="recipient-name" class="col-form-label">Facility Name:</label>
                    <input type="text" class="form-control" name="fname" id="recipient-name">
                  </div>
                  <div class="mb-2">
                    <label for="recipient-name" class="col-form-label">Category:</label>
                    <input type="text" class="form-control" name="category" id="recipient-name">
                  </div>
                  <div class="mb-2">
                    <label for="recipient-name" class="col-form-label">Description:</label>
                    <input type="text" class="form-control" name="description" id="recipient-name">
                  </div>
                  <div class="mb-2">
                    <label for="recipient-name" class="col-form-label">Maximum Booking Duration(Hr):</label>
                    <input type="number" class="form-control" name="maxtime" id="recipient-name">
                  </div>
                  
                  <div class="mb-2">
                    <label for="message-text" class="col-form-label">This facility can be booked from: </label>
                    <input type="text" class="form-control" name="starttime" id="message-text">
                  </div>
                  <div class="mb-2">
                    <label for="message-text" class="col-form-label">To:</label>
                    <input type="text" class="form-control" name="endtime" id="message-text">
                  </div>

                  <div class="mb-2">
                    <label for="message-text" class="col-form-label">Image:</label>
                    <input type="file" class="form-control" name="image" id="message-text">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
              </div>
        </div>
        </div>
      </div>
    </div>
    <div class="user-table">
      <table class="table" >
        <thead >
          <tr>
            <th>
              <span class="custom-checkbox">
                  <input type="checkbox" id="selectAll">
              </span>
            </th>
            <th scope="col">Facility Name</th>
            <th scope="col">Category</th>
            <th scope="col">Image</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>                    
          </tr>
        </thead>
        <tbody>
          @forEach($facilities as $f)
          <tr>
            <td>
              <span class="custom-checkbox">
                  <input type="checkbox" id="{{$f->id}}" name="options[]" value="{{$f->id}}">
              </span>
            </td>
            <td>{{$f->fname}}</td>
            <td>{{$f->category}}</td>
            <td><img style="width: 60px" src="{{ asset('storage/' . str_replace('public/', '', $f->image)) }}" alt="try"></td>
            <td>{{$f->description}}</td>     
            <td>
              <div class="d-flex gap-1">                
                <a class="btn" href="{{ route('facilitystatus', $f->id) }}" style="color:white; background-color: {{ $f->status === 'active' ? '#28a745' : '#dc3545' }}">
                  @if($f->status === 'active')
                    Enabled 
                  @else
                    Disabled
                  @endif
                </a>            
                  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#edit{{$f->id}}">Edit</button>
              </div>
            </td>                        
          </tr>

          <div class="modal fade" id="edit{{$f->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog ">
            <div class="modal-content p-3">
              <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">Edit Facility</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <form action="{{route('facility.edit', $f->id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="mb-2">
                    <label for="recipient-name" class="col-form-label">Facility Name:</label>
                    <input type="text" class="form-control" id="recipient-name" name="fname" value="{{$f->fname}}" required>
                  </div>
                  <div class="mb-2">
                    <label for="recipient-name" class="col-form-label">Category:</label>
                    <input type="text" class="form-control" id="recipient-name" name="category" value="{{$f->category}}" required>
                  </div>
                  <div class="mb-2">
                    <label for="recipient-name" class="col-form-label">Description:</label>
                    <input type="text" class="form-control" id="recipient-name" name="description" value="{{$f->description}}" required>
                  </div>
                  <div class="mb-2">
                    <label for="recipient-name" class="col-form-label">Maximum Booking Time(hr):</label>
                    <input type="text" class="form-control" id="recipient-name" name="maxtime" value="{{$f->maxtime}}" required>
                  </div>
                  <div class="mb-2">
                    <label for="message-text" class="col-form-label">This facility can be booked from: </label>
                    <input type="text" class="form-control" name="starttime" id="message-text" value="{{$f->starttime}}">
                    <label for="message-text" class="col-form-label">To:</label>
                    <input type="text" class="form-control" name="endtime" id="message-text" value="{{$f->endtime}}">
                  </div>
                  <div class="mb-2">
                    <label for="image" class="col-form-label">Current Image:</label>
                        <img style="" src="{{ asset('storage/' . str_replace('public/', '', $f->image)) }}" alt="Facility Image" class="img-thumbnail">
                        <small class="form-text text-muted">Leave this field blank if you don't want to update the image.</small>
                        <input type="file" class="form-control mt-2" id="image" name="image">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" onclick="fireSweetAlertEdit()">Save Changes</button>
                  </div>
                </form>
              </div>                 
            </div>
            </div>
          </div>
        @endforeach      

        </tbody>
      </table>
    </div>

  </div>   

   
    <script>
        $(document).ready(function () {
          $("#selectAll").change(function () {
            $("input[name='options[]']").prop("checked", $(this).prop("checked"));
          });
          $("input[name='options[]']").change(function () {
            if (!$(this).prop("checked")) {
              $("#selectAll").prop("checked", false);
            }
          });
        });
      </script>

<script>
    function fireSweetAlertdelete() {
       const selectedCategoryIds = [];
 
       // Use attribute selector to select checkboxes by id
       $('input[name="options[]"]:checked').each(function () {
          selectedCategoryIds.push($(this).val());
       });
       console.log('Selected Category IDs:', selectedCategoryIds);
 
       if (selectedCategoryIds.length === 0) {
          Swal.fire('Error!', 'Please select at least one facility to delete', 'error');
          return;
       }
 
       Swal.fire({
          title: 'Are you sure you want to delete this facility?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#5cb85c',
          cancelButtonColor: '#d9534f',
          confirmButtonText: 'Yes, delete it!'
       }).then((result) => {
          if (result.isConfirmed) {
             console.log('Before Axios call');
             axios({
                method: 'delete',
                url: 'facility/delete',
                data: { categoryIds: selectedCategoryIds }
             })
             .then((response) => {
                console.log('Axios success', response.data);
                
                setTimeout(() => {
                        location.reload();
                    }, 1500);
                Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        text: response.data.message,
                        showConfirmButton: false
                    });   
             })
             .catch((error) => {
                console.log('Axios error', error);
                Swal.fire('Error!', error.response.data.message, 'error');
             });
             console.log('After Axios call');
          }
       });
    }
</script>
@if(session('success'))
    <script>
        Swal.fire({
            icon: 'success',
            title: 'Success!',
            text: '{{ session('success') }}',
            showConfirmButton: false,
            timer: 1500 // Adjust the time as needed
        });
    </script>
@endif

@if(session('error'))
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Error!',
            text: '{{ session('error') }}',
            showConfirmButton: false,
            timer: 1500 // Adjust the time as needed
        });
    </script>
@endif

</x-admin-sidebar>
