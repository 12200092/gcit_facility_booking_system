<x-admin-sidebar>   
    <div class="admin">
        <div class="about-admin">
            <div style="padding-right:4rem ">
                <h5>GCIT Facility Booking System</h5>
                <p>Welcome to our Facility Booking Admin Panel, your central hub for seamless facility management. Whether you're scheduling 
                events, reserving spaces, or optimizing facility usage, our platform offers intuitive tools to simplify the process. 
                Explore a range of features designed to streamline the booking process, track usage, and enhance overall efficiency.
                </p>
            </div>       
            <img src="{{ asset('asset/images/meeting.png') }}" alt="try">
        </div>
        <div class="admin-cards">
            <div class="card ">
                <div class="inner">
                    <h4>Users</h4>
                    <i class='bx bxs-user'></i>
                </div>             
                <h1>{{ $usercount }}</h1>
            </div>
            <div class="card" style="background-color: #26a843">
                <div class="inner">
                    <h4>Facilities</h4>
                    <i class='bx bxs-category' style="color: #24963e"></i>
                </div> 
                <h1>{{ $facilitycount }}</h1>
            </div>
            <div class="card" style="background-color: #fec106">
                <div class="inner">
                    <h4>Feedbacks</h4>
                    <i class='bx bxs-message-rounded-dots' style="color: #e6ad06"></i>
                </div>
                <h1>{{ $feedbackcount }}</h1>
            </div>
            <div class="card" style="background-color: #dd3444">
                <div class="inner">
                    <h4>Bookings</h4>
                    <i class='bx bx-calendar' style="color: #bb2e39"></i>
                </div>
                <h1>{{ $bookingcount }}</h1>
            </div>
        </div>

        <div class="last-content">
            <div class="charts">
                <h5 style="color: #183866; font-weight:700; padding:0rem 1rem">Most Booked Facilities</h5>
                <canvas  id="bookingsChart" width="100%"></canvas>
            </div>

            {{-- <div class="carousel"> --}}
                {{-- <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner" >
                        <div class="carousel_text">
                            <h5 style="color: #183866; font-weight:700; ">Facilities</h5>
                            <p>"Unlock Unforgettable Experiences with Our Premium Facilities."</p>
                        </div>
                        <div class="carousel-item active">
                            <img src="{{ asset('asset/images/m.jpg') }}" alt="Image 1">
                            <div class="carousel-caption d-none d-md-block">
                            <p>Some representative placeholder content for the first slide.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('asset/images/bb.jpeg') }}" alt="Image 1">
                            <div class="carousel-caption d-none d-md-block">
                            <p>Some representative placeholder content for the second slide.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('asset/images/fb.jpeg') }}" alt="Image 1">
                            <div class="carousel-caption d-none d-md-block">
                            <p>Some representative placeholder content for the third slide.</p>
                            </div>
                        </div>
                    </div>
                </div> --}}
            {{-- </div> --}}
            <div id="carouselExampleCaptions" class="carousel p-4 slide" data-bs-ride="carousel">
                <div class="carousel-inner ">
                    <div class="carousel_text">
                        <h5 style="color: #183866; font-weight:700; ">Facilities</h5>
                        <p>"Unlock Unforgettable Experiences with Our Premium Facilities."</p>
                    </div>
                    <div class="carousel-item active">
                        <img src="{{ asset('asset/images/fb.jpeg') }}" alt="Image 1">
                        <div class="carousel-caption d-none d-md-block">
                        <p>Sports Facilities(Football Ground & Basketball Courts)</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('asset/images/m.jpg') }}" alt="Image 1">
                        <div class="carousel-caption d-none d-md-block">
                        <p>Meeting Halls</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('asset/images/mphh.jpeg') }}" alt="Image 1">
                        <div class="carousel-caption d-none d-md-block">
                        <p>MPH (Cultural events, Volleyball, badminton, Table Tennis )</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('asset/images/gh.jpeg') }}" alt="Image 1">
                        <div class="carousel-caption d-none d-md-block">
                        <p>Guest Houses</p>
                        </div>
                    </div>
                </div>
              </div>
        </div>    
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            fetch('/bookings/chart')
                .then(response => response.json())
                .then(data => {
                    console.log('Chart Data:', data); // Debugging statement
                    var ctx = document.getElementById('bookingsChart').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: data.labels,
                            datasets: [{
                                label: 'No. of Bookings',
                                data: data.counts,
                                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                                borderColor: 'rgba(75, 192, 192, 1)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });
                })
                .catch(error => console.error('Error fetching chart data:', error)); // Debugging statement
        });
    </script>
</x-admin-sidebar>


{{-- <x-admin-component>
    

    <!-- main sidebar -->
    <div class="main-content">
        
    
        <div class="row g-10">
            <div class="col-xl-6 col-lg-6">
                <div class="panel">
                    <div class="panel-body">
                        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="{{ asset('admin_assets/images/all facility 1.png') }}" class="d-block w-100" alt="slide image">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>All Facilities</h5>
                                        <h3>{{ $subcategorycount}}</h3>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('admin_assets/images/all category.png') }}" class="d-block w-100" alt="slide image">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>All Category</h5>
                                        <h3>{{ $categorycount}}</h3>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('admin_assets/images/all booking.png')}}" class="d-block w-100" alt="slide image">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>All Bookings</h5>
                                        <h3>{{ $bookingcount }}</h3>
                                    </div>
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="col-xl-6 col-lg-6">
                <div class="panel mb-10">
                    <div class="panel-heading">
                        <span>Recent Bookings</span>
                       <a href="{{ url ('admin/booking')}}"> <button type="button" class="btn btn-outline-primary" >View more</button> </a>                       
                    </div>
                    
                    <div class="panel-body">
                        <div class="list-group" style="height: 220px; overflow-y: scroll;">
                            <!-- <a href="#" class="list-group-item list-group-item-action active" aria-current="true">
                                <div class="d-flex w-100 justify-content-between">
                                    <p class="mb-1">12200051.gcit@rub.edu.bt</p>
                                    <small>3 days ago</small>
                                </div>
                                <p class="mb-1">Football Ground</p>
                            </a> -->   
                            @foreach ($bookings as $b) 
                            <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                                <div class="d-flex w-100 justify-content-between">
                                    <p class="mb-1">{{ $b->user->email }}</p>
                                    <!-- <small>3 days ago</small> -->
                                </div>
                                <p class="mb-1">{{ $b->subcategory->facility_name}}</p>
                            </a>
                            @endforeach
                            
                            
                          
                            
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="panel col-xl-12 col-lg-12 mb-10 mt-10">
                <div class="panel-heading">
                    <span>Category</span>
                    <!-- <div>
                        <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" style="background-color: green !important; border-color: green !important">Add</button>
                        <button type="button" class="btn btn-danger btn-sm" >Delete</button>
                        
                    </div> -->
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterLabel">Add Category</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group mb-20">
                                            <label class="form-label" for="exampleInputEmail1">Category name</label>
                                            <input type="text" class="form-control" id="exampleInputTexts">
                                        </div>
                                        
                                        <div class="form-group mb-20">
                                            <label class="form-label" for="exampleInputFile">Upload image</label>
                                            <input class="form-control" type="file" id="exampleInputFile">
                                        </div>
                                        
                                    </form>                                    
                                </div>
                                <div class="modal-footer">
                                    <!-- <a href="#">
                                        <button type="button" class="btn btn-primary">Save</button>
                                    </a> -->
                                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal" aria-label="Close">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
    
            <div class="row g-2 mb-3">
                @foreach($categories as $c)
                <div class="col-xxl-3 col-lg-4 col-sm-6">
                    <div class="card border" style="border-radius: 10px;">
                        <!-- <div class="card-header">Header</div> -->
                        <div class="panel-body">
                            <div class="card text-white">
                                <img src="{{ asset('storage/' . str_replace('public/', '', $c->image)) }}" class="card-img" alt="card image"/>                                            
                                <div class="card-img-overlay">
                                    <div class="text-center">
                                        <h5 class="card-title">{{ $c->category_name }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
               
                
            </div>
    
        </div>
        
        
    </div>
    </x-admin-component> --}}