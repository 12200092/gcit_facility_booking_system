@extends('index')
@section('css')
@endsection
@section('title', 'Home')
@section('content')
<!-- <home section> -->
  <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner vh-100">
      <div class="carousel-item active" data-bs-interval="3000">
        <img src="asset/images/11.png" class="d-block w-100  img-fluid" alt="...">
        <div class="carousel-caption d-none d-md-block" >
            <h1>Book your Facilities <br>Today!</h1>
              <p>Efficiently reserve and manage classrooms, meeting rooms,<br> labs, and sports facilities with our 
                comprehensive software<br> solution.</p>
                <a href="/facilities" class="cta">
                  <span>Book Now</span>
                  <svg viewBox="0 0 13 10" height="10px" width="15px">
                    <path d="M1,5 L11,5"></path>
                    <polyline points="8 1 12 5 8 9"></polyline>
                  </svg>
                </a>
        </div>
      </div>
      <div class="carousel-item" data-bs-interval="3000">
        <img src="asset/images/22.png" class="d-block  w-100" alt="...">
        <div class="carousel-caption d-none d-md-block" >
          <h1>Book your Facilities <br>Today!</h1>
            <p>Efficiently reserve and manage classrooms, meeting rooms,<br> labs, and sports facilities with our 
              comprehensive software<br> solution.</p>
              <a href="/facilities" class="cta">
                <span>Book Now</span>
                <svg viewBox="0 0 13 10" height="10px" width="15px">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
      </div>
      </div>
      <div class="carousel-item" data-bs-interval="3000">
        <img src="asset/images/33.png" class="d-block  w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h1>Book your Facilities <br>Today!</h1>
            <p>Efficiently reserve and manage classrooms, meeting rooms,<br> labs, and sports facilities with our 
              comprehensive software<br> solution.</p>
              <a href="/facilities" class="cta">
                <span>Book Now</span>
                <svg viewBox="0 0 13 10" height="10px" width="15px">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
      </div>
      </div>
      <div class="carousel-item" data-bs-interval="3000">
        <img src="asset/images/44.png" class="d-block  w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h1>Book your Facilities <br>Today!</h1>
            <p>Efficiently reserve and manage classrooms, meeting rooms,<br> labs, and sports facilities with our 
              comprehensive software<br> solution.</p>
              <a href="/facilities" class="cta">
                <span>Book Now</span>
                <svg viewBox="0 0 13 10" height="10px" width="15px">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
        </div>
      </div>
    </div>         
  </div>        

  <!-- <FACILITIES section> -->
  <section class="services" id="services">
    <div class="main-text">
      <h2>Our Facilities</h2>
        <p>Explore our Top Facilities !
    </div>   
       
    <div class="services-content">
      @foreach ($facilities as $f)
          <div class="box">
              <div>
                  <img src="{{ asset('storage/' . str_replace('public/', '', $f->image)) }}" alt="try">
              </div>
              <div class="s-text">
                  <span class="s-span">{{ $f->category }}</span>
                  <h3>{{ $f->fname }}</h3>
                  <div class="line"></div>
                  <p>{{ $f->description }}</p>
              </div>
              <div class="s-label">
                  <span class="one">{{ $f->maxtime }}<span class="one"> Hr(s)</span></span>
                  <span class="two">Max Booking hour</span>
              </div>
          </div>
      @endforeach
  </div>    

  </section>
  <!-- <about section> -->
  <div class="parallax">
    <section class="about" id="about">
      <div class="about-text">
        <h2>About Us</h2>
        <p>GCIT Facility Booking System is a comprehensive software solution designed to streamline and enhance the process of 
          booking various facilities within the Gyalpozhing College campus. Our user-friendly platform offers students, faculty, 
          and staff an efficient way to reserve and manage classrooms, meeting rooms, labs, and sports facilities. By digitizing 
          the facility booking process, we aim to optimize resource allocation, reduce administrative overhead, and improve overall 
          campus utilization. Experience the convenience of our system and book your facilities today!</p>
      </div> 
    </section>
  </div>

  <!-- <feedback section> -->
  <section class="testimonials" id="testimonials">
    <div class="testimonials-text">
        <h2>Testimonials</h2>
          <p>Hear from Users we Trust !</p>
    </div>            
    <!-- Swiper -->
 
    <div class="swiper myCustomSwiper">
      <div class="swiper-wrapper">
        @forEach($feedback as $f)
        <div class="swiper-slide">
            <h2>"</h2>
            <p>{{$f->feedback}}</p>
        </div>
          @endforeach   
      </div>
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-pagination"></div>
    </div>
  </section>

  <!-- <feature section> -->
  @include('include.fetures')

<!-- <contact section> -->
  <section class="contact" id="contact">
    <div class="contact-text">
        <h2>Reach Us</h2>
          <p>Get in touch and let us know how we can help !</p>
    </div>
    <div class="contact-content">
      <div class="maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3537.855356342287!2d89.66679669999999!3d27.5359525!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e19566fa54c4df%3A0x82f8fd359c78d7f5!2sGyalpozhing%20College%20of%20Information%20Technology%20-%20Kabjisa%20Campus!5e0!3m2!1sen!2sbt!4v1697226387980!5m2!1sen!2sbt" 
        width="600" height="400" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>    
      </div>
      <form class="form" action="{{route('feedback')}}" method="POST" enctype="multipart/form-data"> 
        @csrf              
        <h5>Feedback</h5>
          <div class="inputt">
            <textarea name="feedback" id="" cols="30" rows="9" placeholder="Write something ..." required></textarea>
          </div>
        <button type="submit" class="fbtn ">Submit</button>
      </form>
    </div>            
</section>

<!-- <custom js link> -->
<script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>

<script type="text/javascript">
  var swiper2 = new Swiper(".myCustomSwiper", {
      effect: "coverflow",
      // grabCursor: true,
      centeredSlides: true,
      slidesPerView: "auto",
      coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      pagination: {
          el: ".swiper-pagination",
      },    
      keyboard: true,
  });
</script>

<script>
  @if(session('success'))
  toastr.options = {
          "progressBar": true,
          "closeButton": true,
          'positionClass': 'toast-bottom-right',

      };
  toastr.success("{{ session('success') }}");
  @endif
  @if(session('error'))
  toastr.options = {
          "progressBar": true,
          "closeButton": true,
          'positionClass': 'toast-bottom-right',
      };
  toastr.error("{{ session('error') }}");
  @endif
</script>

@endsection


