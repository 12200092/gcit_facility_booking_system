<section class="feature" id="feature">
    <div class="feature-text">
        <h2>Key Features</h2>
        <p>We offer the best Services !</p>
    </div>
    <div class="feature-content">
        <div class="keyfeatures">
            <div style="display: flex; flex-direction: row; margin:1rem 0rem">
                <div class="features-icon">
                    <i class="bx bxs-bell" style="background-color: #efca08"></i>
                </div>
                <div class="feature-t">
                    <h5>Notifications and Alerts</h5>
                    Receive alerts for booking approvals, rejections, or changes in booking status.
                </div>
            </div>
            <div style="display: flex; flex-direction: row; margin:1rem 0rem">
                <div class="features-icon">
                    <i class="bx bx-check" style="background-color: #f59131"></i>
                </div>
                <div class="feature-t">
                    <h5>User-Friendly Interface</h5>
                    Intuitive and easy-to-navigate user interface
                </div>
            </div>
            <div style="display: flex; flex-direction: row; margin:1rem 0rem">
                <div class="features-icon">
                    <i class="bx bxs-calendar" style="background-color: #f53d31"></i>
                </div>
                <div class="feature-t">
                    <h5>Facility Availability and Scheduling</h5>
                    Real-time availability display of facilities<br>
                        Select desired booking dates and times.
                </div>
            </div>
            <div style="display: flex; flex-direction: row; margin:1rem 0rem">
                <div class="features-icon">
                    <i class="bx bxs-message-dots" style="background-color: #2ec1ac"></i>
                </div>
                <div class="feature-t">
                    <h5>Feedback and Rating System</h5>
                    Provide feedback and ratings for facilities they have booked
                </div>
            </div>
            <div style="display: flex; flex-direction: row; margin:1rem 0rem">
                <div class="features-icon">
                    <i class="bx bx-refresh" style="background-color: #be2ec1"></i>
                </div>
                <div class="feature-t">
                    <h5 class="h5">Modifications and Cancellations</h5>
                    Easily cancel or modify their bookings within a specified timeframe
                </div>
            </div>          
        </div>
        <div >
            <img src="asset/images/pic.png" class="img-fluid">
        </div>
    </div>             
    
</section>
