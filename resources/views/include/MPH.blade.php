<div class="box">
    <div>
        <img src="{{ asset('asset/images/fbb.jpeg') }}" alt="Image 1">
    </div>
    <div class="s-text">
        <h3>Badminton</h3>
        <div class="line"></div>
        <p>MPH (badminton) can be booked from 10pm to 12pm and 9am to 10pm on weekdays and weekends respectively.</p>
        <div class="line"></div>
        <p>Max Booking hour: 1 hour</p>
        <button>Book Now</button>
    </div>
</div>
<div class="box">
    <div>
        <img src="asset/images/bb.jpeg" alt="Image 1">
    </div>              
    <div class="s-text">                
        <h3>Volleyball</h3>
        <div class="line"></div> 
        <p>MPH (volleyball) can be booked from 12pm to 10pm and 9am to 10pm on weekdays and weekends respectively.</p>
        <div class="line"></div> 
        <p>Max Booking hour: 1 hour</p>
    </div>         
</div>
<div class="box">
    <div>
        <img src="{{ asset('asset/images/fbb.jpeg') }}" alt="Image 1">
    </div>
    <div class="s-text">
        <h3>Table Tennis</h3>
        <div class="line"></div>
        <p>MPH (table tennis) can be booked from 10pm to 12pm and 9am to 10pm on weekdays and weekends respectively.</p>
        <div class="line"></div>
        <p>Max Booking hour: 1 hour</p>
        <button>Book Now</button>
    </div>
</div>
<div class="box">
    <div>
        <img src="asset/images/bb.jpeg" alt="Image 1">
    </div>              
    <div class="s-text">                
        <h3>Basketball Court</h3>
        <div class="line"></div> 
        <p>MPH (table tennis) can be booked from 10pm to 12pm and 9am to 10pm on weekdays and weekends respectively.</p>
        <div class="line"></div> 
        <p>Max Booking hour: 2 hours</p>
    </div>         
</div>