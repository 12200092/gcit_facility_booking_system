<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>calender</title>
   <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/calendar.css')}}">
   <title></title>    
</head>
<body>
    <div class="bookings">
        <div class="list-group px-4">
            <a href="#" class="list-group-item list-group-item-action active text-center py-2" style="background-color:#183866; border:none;">Bookings available today!</a>
            <a href="#" class="list-group-item list-group-item-action">12 - 1 pm<button class="booking-btn float-end">Book Now</button></a>
            <a href="#" class="list-group-item list-group-item-action">1 - 2 pm<button class="booking-btn float-end">Book Now</button></a>
            <a href="#" class="list-group-item list-group-item-action">2 - 3 pm<button class="booking-btn float-end">Book Now</button></a>
            <a href="#" class="list-group-item list-group-item-action">3 - 4 pm<button class="booking-btn float-end">Book Now</button></a>
            <a href="#" class="list-group-item list-group-item-action">4 - 5 pm<button class="booking-btn float-end">Book Now</button></a>
            <a href="#" class="list-group-item list-group-item-action">5 - 6 pm<button class="booking-btn float-end">Book Now</button></a>
        </div>  
    </div> 
</body>
</html>