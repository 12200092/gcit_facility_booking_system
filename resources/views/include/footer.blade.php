<section >
    <div class="blockcode">       
      <footer class="page-footer">
        <div class="d-flex flex-column mx-auto" style="width: 100%">
          <div class="d-flex flex-wrap justify-content-between">
            <div>
              <a href="" class="d-flex align-items-center p-0 text-white">
                <img alt="logo" src="asset/images/GFBS..png" width="150px" />
              </a>
              <p class="list my-3 pt-4" style="max-width: 400px; color: white;">
                GCIT Facility Booking System is a comprehensive software solution designed to streamline and enhance 
                the process of booking various facilities within the Gyalpozhing College campus. 
              </p>
              <div class="footer-icons">
                <i class="bx bxl-facebook" ></i>
                <i class="bx bxl-instagram" ></i>
                <i class="bx bxl-youtube" ></i>
                <i class="bx bxl-twitter" ></i>
              </div>
            </div>
            <div>
              <p class="h5 mb-4" style="font-weight: 600; font-size: 20px; color: white;">Useful Links</p>
              <ul class="p-0" style="list-style: none; cursor: pointer; font-size:15px">
                <li class="list my-3">
                  <a  href="https://www.gcit.edu.bt/">Gyalpozhing College of Information Technology</a>
                </li>
                <li class="list my-3">
                  <a  href="https://www.citizenservices.gov.bt/g2cportal/ListOfLifeEventComponent">G2C Services</a>
                </li>
                <li class="list my-3">
                  <a  href="https://www.rub.edu.bt/">Royal University of Bhutan</a>
                </li>
                <li class="list my-3">
                  <a  href="http://www.education.gov.bt/">Ministry of Education</a></li>
              </ul>
            </div>
            <div>
              <p class="h5 mb-4" style="font-weight: 600; color: white;">Contact Us At</p>
              <ul class="p-0" style="list-style: none; cursor: pointer; font-size:15px">
                <li class="list my-3">
                  <i class='bx bxs-phone-call'></i>
                  <a  href="" style='padding-left:15px;'> + 975 17301649</a>
                </li>
                <li class="list my-3">
                  <i class='bx bxs-phone-call'></i>
                  <a  href="" style='padding-left:15px;'> + 975 17680509</a>
                </li>
                <li class="list my-3">
                  <i class='bx bxs-envelope'></i>
                  <a  href="" style='padding-left:15px;'> 12200092.gcit@rub.edu.bt</a>
                </li>
                <li class="list my-3">
                  <i class='bx bxs-envelope'></i>
                  <a  href="" style='padding-left:15px;'>12200056.gcit@rub.edu.bt</a>
                </li>
                <li class="list my-3" >
                  <i class='bx bxs-map'></i>
                  <a  href="" style='padding-left:15px;'>Chamjekha, Thimphu, Bhutan</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </section>

  <!-- <end section> -->
  <div class="end">
      <div class="last-text">
          Copyright © 4th year IT Group 9 2023. All Rights Reserved.
      </div>
    </div>
