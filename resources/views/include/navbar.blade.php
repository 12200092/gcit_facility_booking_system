<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body>
    {{-- <nav class="navbar fixed-top navbar-expand-lg" style="padding-top: 0;">
        <div class="container-fluid" style="padding: 22px 15%;background-color: rgba(24, 56, 102, 0.7);backdrop-filter: blur(3px);transition: all .45s ease; background-color: #183866; border-bottom:2px solid #06224A">
            <a class="navbar-brand" href="/home">
                <img src="asset/images/GFBS..png">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0 nbar">
                @if(Route::has('login'))
                @auth
                    <li class="nav-item px-4 ">
                        <a class="nav-link n-item" style="color: white; font-weight: 700;" href='/home'>Home</a>
                    </li>
                    <li class="nav-item px-4 ">
                        <a class="nav-link n-item" style="color: white; font-weight: 700;" href="/facilities">Facilities</a>
                    </li>
                    <li class="nav-item px-4 ">
                        <a class="nav-link n-item" style="color: white; font-weight: 700;" href="/about">About</a>
                    </li>   
                </ul>
                <div class="d-flex ms-auto">
                    <button type="button" class="btn" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="bottom" data-bs-html="true" data-bs-content="" id="popoverTrigger">
                        <i class="bx bxs-bell" style="font-size:25px; color:#F59131"></i>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown ">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span class="profile-initial">{{ Auth::user()->initials }}</span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item"><i class="bx bxs-contact pe-1" style="color:#183866"></i>{{ Auth::user()->name }}</a></li>
                                    <li><a class="dropdown-item"><i class="bx bx-envelope pe-1" style="color:#183866"></i>{{ Auth::user()->email }}</a></li>
                                    <li><hr class="dropdown-divider"></li>
                                    <li><a class="dropdown-item" href="{{route('bookings')}}">My Bookings</a></li>                          
                                    <li><a class="dropdown-item" href="{{route('profile.edit')}}">
                                        {{ __('Profile') }}
                                    </a></li>
                                    <li><hr class="dropdown-divider"></li>
                                    <li class="mb-2">    
                                        <form method="POST" action="{{ route('logout') }}">
                                            @csrf
                                            <a class="logout-btn" href="{{ route('logout')}}" onclick="event.preventDefault(); this.closest('form').submit();">Logout</a>
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
               
            </div>
            @else 
            <a class="login-btn" href="{{route('login')}}">Sign In</a>
            <a class="register-btn" href="{{route('register')}}">Sign Up</a>
            @endauth
            @endif
        </div>
    </nav>

    <div id="popoverContent" style="display: none">
        <h6 class="fw-bold">Notifications</h6><hr>
        <ul class="list-unstyled">
            <li>The football ground booking for 5/5/2023 from 2 to    4 PM, has been cancelled and is now open for booking.</li><hr>
            <li>The basketball court booking for 5/5/2023 from 6  to 7 PM, has been cancelled and is now open for booking.</li><hr>
            <li>We're excited to introduce a new facility! Please take a look and explore.</li><hr>
            <li>The badminton court booking for 5/5/2023 from 6 to 7 PM has been cancelled and is now available for booking.</li><hr>
        </ul>
    </div> --}}

    <nav class="navbar navbar-expand-lg navbar-light p-0 fixed-top">
        <div class="container-fluid" style="padding:20px 15%;background-color: rgba(24, 56, 102, 0.7); border-bottom:2px solid #06224A">
            <a class="navbar-brand" href="/home">
                <img src="asset/images/GFBS..png">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0 nbar">
                @if(Route::has('login'))
                @auth
                    <li class="nav-item px-4 ">
                        <a class="nav-link n-item" style="color: white; font-weight: 700;" href='/home'>Home</a>
                    </li>
                    <li class="nav-item px-4 ">
                        <a class="nav-link n-item" style="color: white; font-weight: 700;" href="/facilities">Facilities</a>
                    </li>
                    <li class="nav-item px-4 ">
                        <a class="nav-link n-item" style="color: white; font-weight: 700;" href="/about">About</a>
                    </li> 
                    
                    <li>
                        <button  class=" position-relative bg-transparent border-0 py-2 mx-3" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="bottom" data-bs-html="true"  id="popoverTrigger">
                            <i class="bx bxs-bell" style="font-size:25px; color:#F59131"></i>
                            @php
                                $unreadCount = auth()->user()->unreadNotifications->count();
                            @endphp
                            @if ($unreadCount >= 0)
                            <span style="color: white; font-size:10px" class="position-absolute top-10 start-100 translate-middle px-1 bg-danger rounded-circle">
                                {{ $unreadCount }}
                            </span>
                            @endif
                        </button>
                    </li>
                    
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <span class="profile-initial">{{ Auth::user()->initials }}</span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" ><i class="bx bxs-contact pe-1" style="color:#183866"></i>{{ Auth::user()->name }}</a></li>
                            <li><a class="dropdown-item" ><i class="bx bx-envelope pe-1" style="color:#183866"></i>{{ Auth::user()->email }}</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="{{route('bookings')}}" style="font-weight:600">My Bookings</a></li>                          
                            <li><a class="dropdown-item" href="{{route('profile.edit')}}" style="font-weight:600">
                                {{ __('Profile') }}
                            </a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li class="mb-2">    
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <a class="logout-btn" href="{{ route('logout')}}" onclick="event.preventDefault(); this.closest('form').submit();">Logout</a>
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>    
            </div>  
            @else                      
                <a class="mx-2 login-btn" href="{{route('login')}}">Sign In</a>
                <a class="mx-2 register-btn" href="{{route('register')}}">Sign Up</a>            
            @endauth
            @endif 
        </div>
    </nav>
    <div class="overflow-auto" id="popoverContent" style="display: none">
        @auth
        <h6 class="fw-bold">Notifications</h6><hr>
            @foreach(auth()->user()->notifications as $notification)
                <div >
                    {{ $notification->data['message'] }}
                </div><hr>
            @endforeach
        @else
        <p>User not authenticated</p>
        @endauth
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    
    <script>
        $(document).ready(function () {
            var currentPage = window.location.pathname;

            $('.navbar-nav .nav-link').each(function () {
                var pageHref = $(this).attr('href');

                if (currentPage === pageHref) {
                    $(this).addClass('active-page');
                }
            });

            var popoverTrigger = document.getElementById("popoverTrigger");
            var popoverContent = document.getElementById("popoverContent");

            var popover = new bootstrap.Popover(popoverTrigger, {
                content: popoverContent.innerHTML,
                html: true,
            });

            popoverTrigger.addEventListener("click", function () {
                popover.show();
            });

        });
    </script>
</body>
</html>
