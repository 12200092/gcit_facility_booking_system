@extends('index')
@section('title', 'Facilities')
@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/facilities.css')}}">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
@endsection

@section('content')
<section class="facilityy">
    <div class="facility-content">
    @foreach ($facilities as $f)
    @if ($f->status === 'active')
    <div class="box">
        <div>
            <img src="{{ asset('storage/' . str_replace('public/', '', $f->image)) }}" alt="try">
        </div>
        <div class="s-text">
            <span class="s-span">{{ $f->category }}</span>
            <h3>{{ $f->fname }}</h3>
        <div class="line"></div>
            <p>{{ $f->description }}</p>
            <button type="button" class="mb-3 booking-btn" data-bs-toggle="modal" data-bs-target="#bookingModal{{ $f->id }}" onclick="setupTimeOptions({{ $f->id }})">Book Now</button>
        </div>
        <div class="s-label">
        <span class="one">{{ $f->maxtime }}<span class="one"> Hr(s)</span></span>
        <span class="two">Max Booking hour</span>
        </div>
    </div>
    @endif

    <div class="modal fade" id="bookingModal{{ $f->id }}" tabindex="-1" data-bs-backdrop="static" aria-labelledby="exampleModalLabel{{ $f->id }}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color: #183866; font-weight:700" id="exampleModalLabel">{{ $f->fname }} Booking</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('addbooking') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div>
                        <label style="color: black;font-weight:700">Select date:</label>
                        <input type="date" name="bookingdate" id="date{{ $f->id }}" required>
                    </div>
                    <div>
                        <input type="hidden" name="facility_id" id="facility{{ $f->id }}" value="{{ $f->id }}">
                        <label style="color: black;font-weight:700">Select time:</label>
                        <select name='time' class="form-select" aria-label="Default select example" id="timeSelect{{ $f->id }}">
                        </select> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Confirm Booking</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    @endforeach
    </div>
    
    <div class="recent-bookings">
        <h5>Recent Bookings</h5>
        <p>"The following bookings have already been reserved and are currently unavailable for further reservations."</p>
        <table class="table" >
          <thead >
            <tr>
              <th scope="col">Facility Name</th>
              <th scope="col">Booked by</th>
              <th scope="col">Booked Date</th>
              <th scope="col">Booked Time</th>
            </tr>
          </thead>
          <tbody>
              @forEach($bookings as $b)
            <tr>
              <td>{{$b->facility->fname}}</td>
              <td>{{$b->user->name}}</td>
              <td>{{$b->bookingdate}}</td>
              <td>{{$b->starttime}} to {{$b->endtime}}</td>
            </tr>
            @endforeach      
          </tbody>
        </table>
    </div>
    
    <script>
    const currentDate = new Date().toISOString().split('T')[0];
    // Set the minimum date for the input field
    @foreach ($facilities as $f)
    const startDateInput{{ $f->id }} = document.getElementById('date{{ $f->id }}');
    startDateInput{{ $f->id }}.min = currentDate; 
    @endforeach
    </script>

    @if(session('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: '{{ session('success') }}',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    @if(session('error'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: '{{ session('error') }}',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    
@foreach ($facilities as $f)
    <script>
        function fetchData{{ $f->id }}() {
            var subfacilityId = document.getElementById('facility{{ $f->id }}').value;
            var startDate = document.getElementById('date{{ $f->id }}').value;

            if (startDate) {
                var url = '/getTime/' + subfacilityId + '?start_date=' + startDate;

                fetch(url)
                    .then(response => response.json())
                    .then(data => {
                        console.log(data);

                        const startSelect = document.getElementById('timeSelect{{ $f->id }}');
                        startSelect.innerHTML = '';

                        var startTime = data.session_start_time;
                        var endTime = data.session_end_time;
                        var slot = data.slot; // Declare slot here
                        var bookingStartTimes = data.booking_start_times;
                        var bhutanCurrentTime = data.bhutan_current_time;

                        const bhutanTimeOptions = {
                            timeZone: 'Asia/Thimphu',
                            hour12: false,
                            hour: '2-digit',
                            minute: '2-digit',
                            second: '2-digit'
                        };

                        function generateTimeOptions1(startTime,endTime, slot, bookingStartTimes) {
                                    const [startHour, startMin, startSec] = startTime.split(':').map(Number);
                                    const [endHour, endMin, endSec] = endTime.split(':').map(Number);
                                    const slotHour = slot;
                                    const slotMin = 0;
                    
                                    let currentTime = new Date();
                                    currentTime.setHours(startHour, startMin, startSec);
                    
                                    const endTimeObj = new Date();
                                    endTimeObj.setHours(endHour, endMin, endSec);
                    
                                    const slotIncrement = (slotHour * 60 + slotMin) * 60000;
                    
                                    const bhutanCurrentTime = new Date().toLocaleTimeString('en-US', {
                                            hour12: false,
                                            hour: '2-digit',
                                            minute: '2-digit',
                                            second: '2-digit',
                                            timeZone: 'Asia/Thimphu' // Get current time in Bhutan's timezone
                                        });
                    
                                    console.log('bhutanCurrentTime',bhutanCurrentTime)
                    
                                    while (currentTime <= endTimeObj) {
                                        const option = document.createElement('option');
                                        const endTimeRange = new Date(currentTime.getTime() + slotIncrement);
                    
                                        if (endTimeRange > endTimeObj) {
                                            break;
                                        }
                    
                                        const startTimeStr = currentTime.toLocaleTimeString('en-US', {
                                            hour12: false,
                                            hour: '2-digit',
                                            minute: '2-digit',
                                            second: '2-digit',
                                            timeZone: 'Asia/Thimphu' // Set Bhutan's timezone
                                        });
                    
                                        console.log('startTimeStr',startTimeStr)
                    
                                        if (startTimeStr <= bhutanCurrentTime) {
                                            currentTime = endTimeRange;
                                            continue; // Skip options with startTimeStr <= current time in Bhutan
                                        }
                    
                                        if (!data.booking_start_times.includes(startTimeStr)) {
                                            
                                            const label = startTimeStr + ' - ' + endTimeRange.toLocaleTimeString('en-US', {
                                                hour12: false,
                                                hour: '2-digit',
                                                minute: '2-digit',
                                                second: '2-digit'
                                            });
                    
                                            const value = startTimeStr + ',' + endTimeRange.toLocaleTimeString('en-US', {
                                                hour12: false,
                                                hour: '2-digit',
                                                minute: '2-digit',
                                                second: '2-digit'
                                            });
                    
                                            option.text = label;
                                            option.value = value;
                    
                                            startSelect.add(option);
                                        }
                    
                                        currentTime = endTimeRange;
                                    }
                                }
                                function generateTimeOptions(startTime,endTime,slot , bookingStartTimes, bhutanCurrentTime) {
                                    const [startHour, startMin, startSec] = startTime.split(':').map(Number);
                                    const [endHour, endMin, endSec] = endTime.split(':').map(Number);
                                    const slotHour = slot;
                                    const slotMin = 0;

                                    let currentTime = new Date();
                                    currentTime.setHours(startHour, startMin, startSec);
                    
                                    const endTimeObj = new Date();
                                    endTimeObj.setHours(endHour, endMin, endSec);
                    
                                    const slotIncrement = (slotHour * 60 + slotMin) * 60000;
                    
                                    while (currentTime <= endTimeObj) {
                                        const option = document.createElement('option');
                                        const endTimeRange = new Date(currentTime.getTime() + slotIncrement);
                    
                                        if (endTimeRange > endTimeObj) {
                                            break;
                                        }
                    
                                        const startTimeStr = currentTime.toLocaleTimeString('en-US', {
                                            hour12: false,
                                            hour: '2-digit',
                                            minute: '2-digit',
                                            second: '2-digit'
                                        });
                    
                                        if (!data.booking_start_times.includes(startTimeStr)) {
                                            
                                            const label = startTimeStr + ' - ' + endTimeRange.toLocaleTimeString('en-US', {
                                                hour12: false,
                                                hour: '2-digit',
                                                minute: '2-digit',
                                                second: '2-digit'
                                            });
                    
                                            const value = startTimeStr + ',' + endTimeRange.toLocaleTimeString('en-US', {
                                                hour12: false,
                                                hour: '2-digit',
                                                minute: '2-digit',
                                                second: '2-digit'
                                            });
                    
                                            option.text = label;
                                            option.value = value;
                    
                                            startSelect.add(option);
                                        }
                    
                                        currentTime = endTimeRange;
                                    }
                                }

                        const now = new Date();
                        const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

                        const selectedDate = new Date(startDate);
                        const formattedStartDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate());

                        if (formattedStartDate.getTime() === today.getTime()) {
                            generateTimeOptions1(startTime, endTime, slot, bookingStartTimes); // Pass startTime, endTime, and slot to the function
                        } else {
                            generateTimeOptions(startTime, endTime, slot, bookingStartTimes, bhutanCurrentTime); // Pass startTime, endTime, and slot to the function
                        }
                    })
                    .catch(error => console.error('Error:', error));
            }
        }

        document.getElementById('date{{ $f->id }}').addEventListener('change', function () {
            fetchData{{ $f->id }}();
        });
    </script>
@endforeach

</section>
@endsection
