@extends('index')
@section('title','about')
@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/abt.css')}}">
@endsection
@section('content')
<div class="abt-banner">
    <img src="asset/images/11.png">
    <h1>About Us</h1>
</div>
<section class="abt-page">
<div class="abt-content">
    <div class="inner-content">
        <img src="asset/images/gp.png" class="img-fluid" style="border-radius: 20px">
        <div class="abt-text">
            <h2>GCIT Facility Boooking System</h2>
            <p>Welcome to GCIT Facility Booking - your premier destination for hassle-free and efficient facility reservations right at your fingertips.
                Our goal is to become the go-to solution for all facility-related needs, enhancing the overall college experience and supporting the diverse
                activities that contribute to personal and academic growth.</p>
        </div>
    </div>
    
<div class="inner-content2">
    <div class="abt-text">
        <h2>Mission</h2>
        <p>Our mission is to provide a seamless and convenient platform that empowers students, faculty, and staff to 
            effortlessly reserve and utilize the college's diverse facilities. .</p>
    </div>
    <div class="mission-img" >
        <img src="asset/images/Mission.png" class="img-fluid">
    </div> 
</div>

<div class="inner-content2">
    <div class="vision-img">
            <img src="asset/images/Vision.png" class="img-fluid">
        </div>
        <div class="abt-text">
            <h2>Vision</h2>
            <p>Our vision is to revolutionize the way individuals interact with and make use of the 
                college's facilities. We aspire to be a catalyst for innovation, enabling users to explore 
                opportunities beyond the classroom. </p>
        </div>  
    </div>    
</div>

    <div class="gallery-container">
        <h2 class="title">
            <span class="primary">Meet our Team</span>
            <span class="secondary">
                "Meet the group of Experts Behind Our Success"
            </span>
        </h2>
        <div class="gallery-wrapper">
            <figure class="gallery-item">
                <img src="asset/images/td.jpeg" alt="" class="item-image" />
                <figcaption class="item-description">
                    <h2 class="name">Tshewang Dechen</h2>
                    <span class="role">1220092.gcit@rub.edu.bt</span>
                </figcaption>
            </figure>
            <figure class="gallery-item">
                <img src="asset/images/kp.jpeg" alt="" class="item-image" />
                <figcaption class="item-description">
                    <h2 class="name">Kinga Penjor</h2>
                    <span class="role">1220056.gcit@rub.edu.bt</span>
                </figcaption>
            </figure>
            <figure class="gallery-item">
                <img src="asset/images/mt.jpeg" alt="" class="item-image" />
                <figcaption class="item-description">
                    <h2 class="name">Pema Dendup</h2>
                    <span class="role">12200070.gcit@rub.edu.bt</span>
                </figcaption>
            </figure>
            <figure class="gallery-item">
                <img src="asset/images/kopi.jpg" alt="" class="item-image" />
                <figcaption class="item-description">
                    <h2 class="name">Kopila Pradhan</h2>
                    <span class="role">1220060.gcit@rub.edu.bt</span>
                </figcaption>
            </figure>
            <figure class="gallery-item">
                <img src="asset/images/kes.jpg" alt="" class="item-image" />
                <figcaption class="item-description">
                    <h2 class="name">Kesang Choden Dorji</h2>
                    <span class="role">1220054.gcit@rub.edu.bt</span>
                </figcaption>
            </figure>
        </div>
    </div>
</section>     
    
@endsection
