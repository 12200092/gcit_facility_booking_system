<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title', 'layout')</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css" />
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/testimonials.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/abt.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/footer.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/navbar.css')}}">

        @yield('css')
    </head>
    
    <body>
    @include('include.navbar')
          
    @yield('content')       
    @include('include.footer')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>    </body>
</html>

