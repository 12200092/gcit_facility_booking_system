<div class="sidebar">
    <div class="sidebar-logo">
        <img src="{{ asset('/asset/images/GFBS..png') }}">    
        <span>Administration Panel</span>
    </div>
    <div class="sidebar-menu">
        <div class="menu">
            <div class="menu-item">
                <a href="{{ url ('admin/dashboard')}}" class="{{ Request::is('admin/dashboard') ? 'active' : '' }}"> 
                    <i class="bx bx-home-alt icon"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
            </div>
            <div class="menu-item">
                <a href="{{ url ('admin/facility')}}" class="{{ Request::is('admin/facility') ? 'active' : '' }}"> 
                    <i class="bx bxs-category icon"></i>
                    <span class="nav-text">Facilities</span>
                </a>
            </div> 
            <div class="menu-item">
                <a href="{{ url ('admin/booking')}}" class="{{ Request::is('admin/booking') ? 'active' : '' }}"> 
                    <i class='bx bxs-calendar-alt icon'></i>                
                    <span class="nav-text">Bookings</span>
                </a>
            </div>  
            <div class="menu-item">
                <a href="{{ url ('admin/users')}}" class="{{ Request::is('admin/users') ? 'active' : '' }}"> 
                    <i class='bx bxs-user-detail icon'></i>                
                    <span class="nav-text">Users</span>
                </a>
            </div> 
            <div class="menu-item">
                <a href="{{ url ('admin/feedback')}}" class="{{ Request::is('admin/feedback') ? 'active' : '' }}"> 
                    <i class='bx bxs-message-rounded-dots icon' ></i>                
                    <span class="nav-text">Feedback</span>
                </a>
            </div>
        </div>

                
    </div>               
</div>

<script>
    $(document).ready(function () {
        var currentPage = window.location.pathname;

        $('.sidebar-menu .menu-item').each(function () {
            var pageHref = $(this).find('a').attr('href');

            if (currentPage === pageHref) {
                $(this).addClass('active');
            }
        });
    });
</script>

