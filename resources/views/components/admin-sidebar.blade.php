<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin Panel</title>
    {{-- <link href="{{ asset('asset/images/GFBS..png') }}" rel="icon"> --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/sidebar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/users.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('./asset/css/admin-dashboard.css')}}">

    <!-- Include Chart.js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

</head>
<body>
    <div style="display:flex;">
        @include('layouts.sidebar')
        <main class="main-content">
            <div class="admin-header">
                <h5 class="ms-4">GCIT Facility Booking Administration</h5>
                <div class="dropdown d-flex align-items-center me-4">
                    <a style="color: white" class="btn btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                  <span >{{ Auth::user()->name }}</span>
                    </a>             
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <li><a style="font-weight:bold;font-size:14px" class="dropdown-item" href="">Name: {{ Auth::user()->name }}</a></li>
                        <li><a style="font-weight:bold;font-size:14px" class="dropdown-item" href="">Email: {{ Auth::user()->email }}</a></li>
                        <li><a class="dropdown-item" href="{{route('profile.edit')}}">
                            {{ __('Profile') }}
                        </a></li>
                        <hr>
                        <li class="mb-2">
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <a href="{{ route('logout')}}" onclick="event.preventDefault();
                                this.closest('form').submit();" >
                                    <span class="admin-logout ">Log Out</span>               
                                </a>
                            </form> 
                        </li>
                    </ul>
                  </div>
            </div>         

            {{ $slot }}
        </main>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

</body>
</html>

