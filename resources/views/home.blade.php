@extends('welcome')
@section('css')
@endsection
@section('title', 'Home')
@section('content')
<!-- <home section> -->
  <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner" style="height: 100vh; ">
      <div class="carousel-item active" style="height: 100vh;" data-bs-interval="3000">
        <img src="asset/images/11.png" class="d-block w-100  img-fluid" alt="...">
        <div class="carousel-caption m-0" >
            <h1>Book your Facilities <br>Today!</h1>
              <p>Efficiently reserve and manage classrooms, meeting rooms,<br> labs, and sports facilities with our 
                comprehensive software<br> solution.</p>
                <a href="/facilities" class="cta">
                  <span>Book Now</span>
                  <svg viewBox="0 0 13 10" height="10px" width="15px">
                    <path d="M1,5 L11,5"></path>
                    <polyline points="8 1 12 5 8 9"></polyline>
                  </svg>
                </a>
        </div>
      </div>
      <div class="carousel-item" style="height: 100vh;" data-bs-interval="3000">
        <img src="asset/images/22.png" class="d-block  w-100" alt="...">
        <div class="carousel-caption" >
          <h1>Book your Facilities <br>Today!</h1>
            <p>Efficiently reserve and manage classrooms, meeting rooms,<br> labs, and sports facilities with our 
              comprehensive software<br> solution.</p>
              <a href="/facilities" class="cta">
                <span>Book Now</span>
                <svg viewBox="0 0 13 10" height="10px" width="15px">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
      </div>
      </div>
      <div class="carousel-item" style="height: 100vh;" data-bs-interval="3000">
        <img src="asset/images/33.png" class="d-block  w-100" alt="...">
        <div class="carousel-caption" >
          <h1>Book your Facilities <br>Today!</h1>
            <p>Efficiently reserve and manage classrooms, meeting rooms,<br> labs, and sports facilities with our 
              comprehensive software<br> solution.</p>
              <a href="/facilities" class="cta">
                <span>Book Now</span>
                <svg viewBox="0 0 13 10" height="10px" width="15px">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
      </div>
      </div>
      <div class="carousel-item" style="height: 100vh;" data-bs-interval="3000">
        <img src="asset/images/44.png" class="d-block  w-100" alt="...">
        <div class="carousel-caption" >
          <h1>Book your Facilities <br>Today!</h1>
            <p>Efficiently reserve and manage classrooms, meeting rooms,<br> labs, and sports facilities with our 
              comprehensive software<br> solution.</p>
              <a href="/facilities" class="cta">
                <span>Book Now</span>
                <svg viewBox="0 0 13 10" height="10px" width="15px">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
      </div>
      </div>
    </div>         
  </div>        

  <!-- <FACILITIES section> -->
  <section class="services" id="services">
    <div class="main-text">
      <h2>Our Facilities</h2>
        <p>Explore our Top Facilities !
    </div>      

    {{-- <div class="services-content">
      <div class="box">
        <div>
          <img src="asset/images/fbb.jpeg" alt="Image 1">
        </div>              
        <div class="s-text">
          <span class="s-span">Sports</span>                                            
          <h3>Football Ground</h3>
          <div class="line"></div> 
          <p>Football grounds can be booked from 12pm to 6pm and 9am to 6pm on weekdays and  weekends respectively.</p>
        </div>
        <div class="s-label">
          <span class="one">2 Hrs</span>
          <span class="two">Max Booking hour</span>
        </div>          
      </div>

      <div class="box">
        <div>
          <img src="asset/images/m.jpg" alt="Image 1">
        </div>              
        <div class="s-text">
          <span class="s-span">Meeting Hall</span>                                            
          <h3>Meeting Halls</h3>
          <div class="line"></div> 
          <p>Meeting Halls can be booked from and 9am to 5pm on weekdays. Bookings not available during weekends.</p>
        </div>
        <div class="s-label">
          <span class="one">2 Hrs</span>
          <span class="two">Max Booking hour</span>
        </div>          
      </div>

      <div class="box">
        <div>
          <img src="asset/images/vbb.jpeg" alt="Image 1">
        </div>              
        <div class="s-text">   
          <span class="s-span">Meeting Hall</span>                                                         
          <h3>MPH (volleyball)</h3>
          <div class="line"></div> 
          <p>MPH (volleyball) can be booked from 12pm to 10pm and 9am to 10pm on weekdays and weekends respectively.<br></p>
        </div>
        <div class="s-label">
          <span class="one">2 Hrs</span>
          <span class="two">Max Booking hour</span>
        </div>         
      </div>

      <div class="box">
        <div>
          <img src="asset/images/bb.jpeg" alt="Image 1">
        </div> 
        <div class="s-text"> 
          <span class="s-span">Sports</span>                            
          <h3>Basketball Court</h3>
          <div class="line"></div> 
          <p>Basketball courts can be booked from 12pm to 6pm and 9am to 6pm on weekdays and weekends respectively.</p>
        </div> 
        <div class="s-label">
          <span class="one">1 Hr</span>
          <span class="two">Max Booking hour</span>
        </div>         
      </div>
      
      <div class="box">
        <div>
          <img src="asset/images/2323.jpeg" alt="Image 1">
        </div>              
        <div class="s-text">
          <span class="s-span">Guest Houses</span>                                                            
          <h3>Guest Houses</h3>
          <div class="line"></div> 
          <p>Guest houses can be booked every day .<br><br><br></p>
        </div>  
        <div class="s-label">
          <span class="one">2 Hrs</span>
          <span class="two">Max Booking hour</span>
        </div>       
      </div>
      
      <div class="box">
        <div>
          <img src="asset/images/c.jpeg" alt="Image 1">
        </div>              
        <div class="s-text">   
          <span class="s-span">Meeting Hall</span>                                                                      
          <h3>MPH (Events & Concerts)</h3>
          <div class="line"></div> 
          <p>MPH (Events & Concerts) can be booked from 12pm to 10pm and 9am to 10pm on weekdays and weekends respectively.</p>
        </div>  
        <div class="s-label">
          <span class="one">2 Hrs</span>
          <span class="two">Max Booking hour</span>
        </div>        
      </div>

      <button class="e-btn" id="explore-btn">
        <span>Explore More</span> 
      </button>

      <div class="box extra" style="display: none;">
        <div>
          <img src="asset/images/fbb.jpeg" alt="Image 1">
        </div>              
        <div class="s-text"> 
          <span class="s-span">Meeting Hall</span>                                                                        
          <h3>Badminton</h3>
          <div class="line"></div> 
          <p>MPH (badminton) can be booked from 10pm to 12pm and 9am to 10pm on weekdays and weekends respectively.</p>
        </div>    
        <div class="s-label">
          <span class="one">2 Hrs</span>
          <span class="two">Max Booking hour</span>
        </div>      
      </div>

      <div class="box extra" style="display: none;">
        <div>
          <img src="asset/images/fbb.jpeg" alt="Image 1">
        </div>              
        <div class="s-text">    
          <span class="s-span">Meeting Hall</span>                                                                     
          <h3>Table Tennis</h3>
          <div class="line"></div> 
          <p>MPH (table tennis) can be booked from 10pm to 12pm and 9am to 10pm on weekdays and weekends respectively.</p>
        </div> 
        <div class="s-label">
          <span class="one">2 Hrs</span>
          <span class="two">Max Booking hour</span>
        </div>         
      </div> --}}

  </section>

  <!-- <about section> -->
  <div class="parallax">
    <section class="about" id="about">
      <div class="about-text">
        <h2>About Us</h2>
        <p>GCIT Facility Booking System is a comprehensive software solution designed to streamline and enhance the process of 
          booking various facilities within the Gyalpozhing College campus. Our user-friendly platform offers students, faculty, 
          and staff an efficient way to reserve and manage classrooms, meeting rooms, labs, and sports facilities. By digitizing 
          the facility booking process, we aim to optimize resource allocation, reduce administrative overhead, and improve overall 
          campus utilization. Experience the convenience of our system and book your facilities today!</p>
      </div> 
    </section>
  </div>

  <!-- <feedback section> -->
  <section class="testimonials" id="testimonials">
    <div class="testimonials-text">
        <h2>Testimonials</h2>
          <p>Hear from Users we Trust !</p>
    </div>            
    <!-- Swiper -->
    {{-- <div class="swiper myCustomSwiper">
      <div class="swiper-wrapper">
          <div class="swiper-slide">
              <h2>"</h2>
              <p>The booking process was relatively straightforward. However, I had a 
                bit of trouble finding the cancel reservation option. Maybe you could make it 
                more prominent on the dashboard.</p>
          </div>
          <div class="swiper-slide">
            <h2>"</h2>
              <p>Do you have plans to launch a mobile app? It would be fantastic to have access to the booking system on the go.</p>
          </div>
          <div class="swiper-slide">
            <h2>"</h2>
              <p>I contacted your support team once, and they were very responsive and helpful. Kudos!</p>
          </div>
          <div class="swiper-slide">
            <h2>"</h2>
              <p>Allow users to set up recurring bookings for facilities, especially for weekly meetings or classes. 
                Include a "Help" or "FAQ" section within the platform for common user queries.</p>
          </div>
          <div class="swiper-slide">
            <h2>"</h2>
              <p>The booking process was relatively straightforward. However, I had a 
                bit of trouble finding the cancel reservation option. Maybe you could make it 
                more prominent on the dashboard.</p>
          </div>
          <div class="swiper-slide">
            <h2>"</h2>
              <p>The booking process was relatively straightforward. However, I had a 
                bit of trouble finding the cancel reservation option. Maybe you could make it 
                more prominent on the dashboard.</p>
          </div>
          <div class="swiper-slide">
            <h2>"</h2>
              <p>The booking process was relatively straightforward. However, I had a 
                bit of trouble finding the cancel reservation option. Maybe you could make it 
                more prominent on the dashboard.</p>
          </div>
      </div>
      <div class="swiper-pagination"></div>
    </div> --}}
  </section>

  <!-- <feature section> -->
  @include('include.fetures')

  

<!-- <contact section> -->
  <section class="contact" id="contact">
    <div class="contact-text">
        <h2>Reach Us</h2>
          <p>Get in touch and let us know how we can help !</p>
    </div>
    <div class="contact-content">
      <div class="maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3537.855356342287!2d89.66679669999999!3d27.5359525!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e19566fa54c4df%3A0x82f8fd359c78d7f5!2sGyalpozhing%20College%20of%20Information%20Technology%20-%20Kabjisa%20Campus!5e0!3m2!1sen!2sbt!4v1697226387980!5m2!1sen!2sbt" 
        width="600" height="400" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>    
      </div>

      <form class="form">               
        <h5>Feedback</h5>
          <div class="inputt">
            <textarea name="" id="" cols="30" rows="9" placeholder="Write something ..." required></textarea>
          </div>
        <button type="submit" class="fbtn ">Submit</button>
      </form>
    </div>            
</section>

<!-- <custom js link> -->
<script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>

<script type="text/javascript">
  var swiper2 = new Swiper(".myCustomSwiper", {
      effect: "coverflow",
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: "auto",
      coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true,
      },
      pagination: {
          el: ".swiper-pagination",
      },
  });


  $(document).ready(function () {
      // Define a flag to track the state of extra cards
      let extraCardsVisible = false;

      // Handle the "Explore More" button click
      $("#explore-btn").click(function () {
          // Toggle the visibility of the extra cards
          $(".box.extra").toggle();

          // Update the button text based on the card state
          if (extraCardsVisible) {
              $("#explore-btn").text("Explore More");
          } else {
              $("#explore-btn").text("Show Less");
          }

          // Toggle the flag
          extraCardsVisible = !extraCardsVisible;
      });
  });
  
</script>
@endsection